#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

from flask_echo_server.app import app

if __name__ == "__main__":
    port = os.environ.get('ECHO_PORT', 5500)
    host = os.environ.get('HOST', '127.0.0.1')
    app.run(host=str(host), port=int(port))
