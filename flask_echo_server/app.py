# -*- coding: utf-8 -*-
import logging

from flask import Flask

logging.basicConfig( level=logging.INFO, format='[%(asctime)s] [%(levelname)s] %(message)s')

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return '/%s' % path
